# Custom error message Zend Framework #
[Custom error message Zend Framework](https://bitbucket.org/AurelioDeRosa/custom-error-message-zend-framework) is a demo that shows how to mark a field of a `Zend_Form`, which is a `Zend_Form_Element`, as invalid showing one or more custom error messages in Zend Framework version 1.12 and below. This demo refers to my article titled ["Setting Custom Error Messages for Zend_Form_Element"](http://phpmaster.com/setting-custom-error-messages-for-zend_form_element/) published on [PHPMaster.com](http://phpmaster.com).

## Requirements ##
Being a demo for Zend Framework 1.12 and below, the only requirement is the [Zend Framework](http://framework.zend.com). To keep this repository as light as possible, the framework has not been included. So, to execute the demo, [download](http://framework.zend.com) one of the cited version of Zend Framework 1 and copy the `library` folder into the root of this repository, and that is at the same level of the `application` and `public` folders.

## Usage ##
To run this demo, simply point your browser to the path where you have stored the repository.

## License ##
[Custom error message Zend Framework](https://bitbucket.org/AurelioDeRosa/custom-error-message-zend-framework) is dual licensed under [MIT](http://www.opensource.org/licenses/MIT) and [GPL-3.0](http://opensource.org/licenses/GPL-3.0)

## Authors ##
[Aurelio De Rosa](http://www.audero.it) ([@AurelioDeRosa](https://twitter.com/AurelioDeRosa))