<?php

class IndexController extends Zend_Controller_Action
{
   public function init()
   {
   }

   public function indexAction()
   {
      $form = new Application_Form_User();

      if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost()))
         $this->view->message = 'Valid input';
      else
         $this->view->form = $form;
   }
}