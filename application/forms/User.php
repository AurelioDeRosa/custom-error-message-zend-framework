<?php

class Application_Form_User extends Zend_Form
{
   public function init()
   {
      // The following lines of code will create the field that contains the Name
      // of the user as described above.
      // Create the object that represents the field and assign to its name
      // attribute the value "name"
      $element = new Zend_Form_Element_Text('name');
      // Set the label of the object
      $element->setLabel('Name');
      // Set the validators. The value of the "Name" must have only alphabetic characters,
      // including the space, and its length must be beetween 3 and 50 characters
      $element->setValidators(array(
          array('Alpha', true, array('allowWhiteSpace' => true)),
          array('StringLength', true, array('min' => 3, 'max' => 50))
      ));
      // Set the custom message in the case of an error
      $element->setErrorMessages(array('The input is invalid. The value must have only alphabetic characters, including the space, and its length must be beetween 3 and 50 characters'));
      // Set the field as required
      $element->setRequired();
      // Add the created element to the form
      $this->addElement($element);

      // Add the submit button to the form
      $element = new Zend_Form_Element_Submit('submit');
      $element->setLabel('Submit');
      $this->addElement($element);
   }
}